declare module 'string-args-parser' {

	/**
	 * Parses the given string into an argument array.
	 *
	 * @param {string} args The string to parse.
	 * @return {string[]} The parsed arguments.
	 */
	export function parseArgs(args: string): string[]
}
