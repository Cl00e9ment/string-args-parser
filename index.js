/**
 * Parses the given string into an argument array.
 *
 * @param {string} args The string to parse.
 * @return {string[]} The parsed arguments.
 */
function parseArgs(args) {
	args = args.trim()
	const parsed = []

	while (args.length > 0) {
		if (args.charAt(0) === '"') {
			let parseContent = true

			let end = args.indexOf('" ', 1)
			if (end !== -1) ++end
			else if (args.charAt(args.length - 1) === '"') end = args.length

			if (end === -1) {
				parseContent = false
				end = args.indexOf(' ')
				if (end === -1) end = args.length
			}

			let arg = args.substring(0, end)
			args = args.substring(end).trim()

			if (parseContent) {
				// remove quotes
				arg = arg.substring(1, arg.length - 1)
				// interpret escape sequences
				for (let i = 0; i + 1 < arg.length; ++i) {
					const currentSequence = arg.substring(i, i + 2)
					if (currentSequence === '\\\\' || currentSequence === '\\"') {
						arg = arg.substring(0, i) + arg.substring(i + 1, arg.length)
					}
				}
			}

			parsed.push(arg)

		} else {
			let end = args.indexOf(' ')
			if (end === -1) end = args.length
			parsed.push(args.substring(0, end))
			args = args.substring(end).trim()
		}
	}

	return parsed
}

module.exports = { parseArgs }
