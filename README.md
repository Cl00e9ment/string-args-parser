# String args parser

A small utility to **parse** a **string** into an **argument array** in a really **permissive** way with the possibility to use **quotes**.

```js
import { parseArgs } from 'string-args-parser'

console.log(parseArgs('foo "bar baz" "\\"qux\\" in quotes"'))
// will print: ['foo', 'bar baz', '"qux" in quotes']
```

## Examples

| input                | output                    |
| :------------------- | :------------------------ |
| `foo␣bar␣baz`        | `['foo', 'bar', 'baz']`   |
| `foo␣"bar␣baz"`      | `['foo', 'bar baz']`      |
| `foo␣"bar␣baz"␣""`   | `['foo', 'bar baz', '']`  |
| `foo␣"bar␣\"baz\""`  | `['foo', 'bar "baz"']`    |
| `foo␣"bar␣baz`       | `['foo', '"bar', 'baz']`  |
| `foo␣"bar"baz`       | `['foo', '"bar"baz']`     |
| `foo␣"ba\\r"␣baz`    | `['foo', 'ba\\r', 'baz']` |
| `foo␣"ba\r"␣baz`     | `['foo', 'ba\\r', 'baz']` |
| `␣foo␣␣␣␣bar␣␣baz␣␣` | `['foo', 'bar', 'baz']`   |
| `␣foo␣"␣bar"␣␣␣baz␣` | `['foo', ' bar', 'baz']`  |
