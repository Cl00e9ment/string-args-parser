const { expect } = require('chai')
const { parseArgs } = require('./index')

const testCases = [
	['foo bar baz'        , ['foo', 'bar', 'baz']  ],
	['foo "bar baz"'      , ['foo', 'bar baz']     ],
	['foo "bar baz" ""'   , ['foo', 'bar baz', ''] ],
	['foo "bar \\"baz\\""', ['foo', 'bar "baz"']   ],
	['foo "bar baz'       , ['foo', '"bar', 'baz'] ],
	['foo "bar"baz'       , ['foo', '"bar"baz']    ],
	['foo "ba\\\\r" baz'  , ['foo', 'ba\\r', 'baz']],
	['foo "ba\\r" baz'    , ['foo', 'ba\\r', 'baz']],
	['foo "ba\\"r" baz'   , ['foo', 'ba"r', 'baz'] ],
	[' foo    bar  baz  ' , ['foo', 'bar', 'baz']  ],
	[' foo " bar"   baz ' , ['foo', ' bar', 'baz'] ],
]

describe('testing some conversions', () => {
	for (const [input, expectedOutput] of testCases) {
		it(input, () => {
			expect(parseArgs(input)).to.deep.equal(expectedOutput)
		})
	}
})
